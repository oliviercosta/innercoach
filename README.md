following : https://testdriven.io/blog/dockerizing-django-with-postgres-gunicorn-and-nginx/
-> ignoring the flake script (useless when it breaks on the out-of-the-box django)) 

=> end result should be to render the default Django website at localhost:8000 from within a docker image


to build dev (using defaults)
============================= 
-> execute
docker-compose build 


to build the production env 
===================================
-> you need following 2 files in the root


.env.prod:
----------
DEBUG=0
SECRET_KEY=change_me
DJANGO_ALLOWED_HOSTS=localhost 127.0.0.1 [::1]
SQL_ENGINE=django.db.backends.postgresql
SQL_DATABASE=hello_django_prod
SQL_USER=hello_django
SQL_PASSWORD=hello_django
SQL_HOST=db
SQL_PORT=5432
DATABASE=postgres


.env.prod.db:
-------------
POSTGRES_USER=hello_django
POSTGRES_PASSWORD=hello_django
POSTGRES_DB=hello_django_prod


-> execute
docker-compose -f docker-compose.prod.yml build